package co.za.mpholetsiki.service;

import java.util.List;

import javax.ejb.Local;

import co.za.mpholetsiki.utility.Forecast;
import co.za.mpholetsiki.utility.Money;

@Local
public interface CallDepositService {
  public List<Forecast> getForecast(Money principal, double interestRate, int term);
}
