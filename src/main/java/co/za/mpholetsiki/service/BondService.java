/**
 * 
 */
package co.za.mpholetsiki.service;

import java.util.List;

import javax.ejb.Local;

import co.za.mpholetsiki.utility.Forecast;
import co.za.mpholetsiki.utility.Money;

/**
 * @author mpho.letsiki
 *
 */
@Local
public interface BondService {

  public Money monthlyRepayment(Money capital, double rate, int period);

  public Money monthlyRepayment(Money capital, double rate, int period, Money deposit);

  public List<Forecast> getForecast(Money capital, double rate, int period);
}