package co.za.mpholetsiki.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import co.za.mpholetsiki.utility.Forecast;
import co.za.mpholetsiki.utility.Interest;
import co.za.mpholetsiki.utility.Money;

@Stateless
public class CallDepositServiceBean
    implements CallDepositService {

  @Override
  public List<Forecast> getForecast(Money principal, double interestRate, int term) {
    List<Forecast> forecasts = new ArrayList<>();
    Money closingBalance = principal;
    for (int i = 1; i <= term; i++) {
      Money openingBalance = closingBalance;
      Money interestEarned = openingBalance.multiply(Interest.getMonthlyRate(interestRate));
      closingBalance = openingBalance.add(interestEarned);
      Forecast forecast = new Forecast(i, openingBalance, interestEarned, closingBalance);
      forecasts.add(forecast);
    }
    return forecasts;
  }
}