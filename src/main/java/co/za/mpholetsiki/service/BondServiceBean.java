/**
 * 
 */
package co.za.mpholetsiki.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import co.za.mpholetsiki.utility.Forecast;
import co.za.mpholetsiki.utility.Interest;
import co.za.mpholetsiki.utility.Money;

/**
 * @author mpho.letsiki
 *
 */
@Stateless
public class BondServiceBean
    implements BondService {

  @Override
  public Money monthlyRepayment(Money capital, double rate, int period) {
    rate = Interest.getMonthlyRate(rate);
    double factor = (rate) / (1 - (1 / Math.pow((1 + rate), period)));
    return capital.multiply(factor);
  }

  @Override
  public Money monthlyRepayment(Money capital, double rate, int period, Money deposit) {
    return (monthlyRepayment(capital.subtract(deposit), rate, period));
  }

  @Override
  public List<Forecast> getForecast(Money capital, double rate, int period) {
    List<Forecast> result = new ArrayList<>();
    Money balance = capital;
    Money interest = new Money(0);
    Money loanRepayment = new Money(0);
    Money openingBalance = new Money(0);
    Money closingBalance = new Money(0);
    Money capitalPayment = new Money(0);
    Money monthlyRepayment;

    for (int i = 1; i <= period; i++) {

      monthlyRepayment = monthlyRepayment(balance, rate, period - i + 1);
      interest = balance.multiply(Interest.getMonthlyRate(rate));
      loanRepayment = monthlyRepayment.subtract(interest);
      balance = balance.subtract(loanRepayment);
      openingBalance = balance.add(loanRepayment);
      closingBalance = balance;
      // capitalPayment = capitalPayment.add(loanRepayment);

      Forecast forecast = new Forecast(i, openingBalance, monthlyRepayment, loanRepayment, interest, closingBalance);
      result.add(forecast);
    }

    return result;
  }
}