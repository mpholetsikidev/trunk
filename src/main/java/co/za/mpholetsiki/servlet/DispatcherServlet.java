/**
 * 
 */
package co.za.mpholetsiki.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.za.mpholetsiki.controller.BondController;
import co.za.mpholetsiki.controller.CallDepositController;

/**
 * @author mpho.letsiki
 *
 */
public class DispatcherServlet extends HttpServlet {

  /**
   * 
   */

  private static final long serialVersionUID = 1L;

  public static final Map<String, Object> CONTROLLERS = new HashMap<String, Object>();

  @Inject
  private CallDepositController callDepositController;

  @Inject
  private BondController bondController;

  public void init()
      throws ServletException {
    CONTROLLERS.put("calldeposit", callDepositController);
    CONTROLLERS.put("Bond", bondController);

  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String[] path = req.getPathInfo().split("/");

    try {
      if (path[1].equals("calldeposit")) {
        if (path[2].equals("forecast")) {
          callDepositController.getForecast(req, resp);
        }
      }

      if (path[1].equals("bond")) {
        if (path[2].equals("forecast")) {
          bondController.getForecast(req, resp);
        }
      }
    }
    catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}