package co.za.mpholetsiki.controller;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.za.mpholetsiki.service.CallDepositService;
import co.za.mpholetsiki.utility.Forecast;
import co.za.mpholetsiki.utility.Money;

@Named("CallDepositController")
public class CallDepositController {

  @Inject
  CallDepositService callDepositService;

  public void getForecast(HttpServletRequest req, HttpServletResponse resp) {
    try {
      if (req.getParameter("principal") == null || req.getParameter("term") == null
          || req.getParameter("interest") == null) {
        List<Forecast> forecasts = new ArrayList<Forecast>();
        req.setAttribute("forecasts", forecasts);
        req.setAttribute("showTable", "false");
        req.getRequestDispatcher("/WEB-INF/views/calldeposit.jsp").forward(req, resp);
      }
      else {
        Money principal = new Money(Double.parseDouble(req.getParameter("principal")));
        double interestRate = Double.parseDouble(req.getParameter("interest"));
        int term = Integer.parseInt(req.getParameter("term"));

        req.setAttribute("forecasts", callDepositService.getForecast(principal, interestRate, term));
        req.setAttribute("showTable", "true");
        req.getRequestDispatcher("/WEB-INF/views/calldeposit.jsp").forward(req, resp);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}
