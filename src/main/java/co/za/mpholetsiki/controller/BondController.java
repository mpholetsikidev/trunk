/**
 * 
 */
package co.za.mpholetsiki.controller;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.za.mpholetsiki.service.BondService;
import co.za.mpholetsiki.utility.Money;

/**
 * @author mpho.letsiki
 *
 */
@Named("BondController")
public class BondController {

  @Inject
  BondService bondService;

  public void getForecast(HttpServletRequest req, HttpServletResponse resp) {

    try {
      if (req.getParameter("capital") == null || req.getParameter("period") == null || req.getParameter("rate") == null) {
        // List<Forecast> forecasts = new ArrayList<Forecast>();
        // req.setAttribute("forecasts", forecasts);
        req.setAttribute("showTable", "false");
        // req.getRequestDispatcher("/WEB-INF/views/bond.jsp").forward(req, resp);
      }
      else {
        Money capital = new Money(Double.parseDouble(req.getParameter("capital")));
        double rate = Double.parseDouble(req.getParameter("rate"));
        int period = Integer.parseInt(req.getParameter("period"));

        req.setAttribute("forecasts", bondService.getForecast(capital, rate, period));
        req.setAttribute("showTable", "true");

      }
      req.getRequestDispatcher("/WEB-INF/views/bond.jsp").forward(req, resp);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}