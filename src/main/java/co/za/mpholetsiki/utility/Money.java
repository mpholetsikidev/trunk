/**
 * 
 */
package co.za.mpholetsiki.utility;

import java.math.BigDecimal;

/**
 * @author mpho.letsiki
 *
 */
public class Money {

  private BigDecimal amount;

  public Money(BigDecimal amount) {
    this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  public Money(double amount) {
    this.amount = BigDecimal.valueOf(amount).setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  public Money(int amount) {
    this.amount = BigDecimal.valueOf(amount).setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  public Money add(Money value) {
    return new Money(amount.add(value.getValue()));
  }

  public Money subtract(Money value) {
    return new Money(amount.subtract(value.getValue()));
  }

  public Money multiply(double value) {
    return new Money(amount.multiply(BigDecimal.valueOf(value)));
  }

  public Money divide(double value) {
    return new Money(amount.divide(BigDecimal.valueOf(value)));
  }

  /**
   * @return BigDecimal amount
   */
  public BigDecimal getValue() {
    return amount;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "R " + amount;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((amount == null) ? 0 : amount.hashCode());
    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Money other = (Money) obj;
    if (amount == null) {
      if (other.amount != null) return false;
    }
    else if (!amount.equals(other.amount)) return false;
    return true;
  }
}
