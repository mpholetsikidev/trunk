/**
 * 
 */
package co.za.mpholetsiki.utility;

/**
 * @author mpho.letsiki
 *
 */
public class Interest {

  public static double getMonthlyRate(double rate) {
    return (rate / 100) / 12;
  }

  public static double getDailyRate(double rate) {
    return (rate / 100) / 365;
  }
}