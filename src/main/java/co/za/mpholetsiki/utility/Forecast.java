/**
 * 
 */
package co.za.mpholetsiki.utility;

/**
 * @author mpho.letsiki
 *
 */
public class Forecast {

  private int month;

  private Money openingBalance;

  private Money loanRepayment;

  private Money interest;

  private Money capitalPayment;

  private Money closingBalance;

  private Money monthlyRepayment;

  // currently used for bond forecast
  public Forecast(int month, Money openingBalance, Money loanRepayment, Money interestCharged, Money capitalPayment,
      Money closingBalance) {
    this.month = month;
    this.openingBalance = openingBalance;
    this.loanRepayment = loanRepayment;
    this.interest = interestCharged;
    this.capitalPayment = capitalPayment;
    this.closingBalance = closingBalance;
  }

  // currently used for call deposit forecast
  public Forecast(int month, Money openingBalance, Money interest, Money closingBalance) {
    this.month = month;
    this.openingBalance = openingBalance;
    this.interest = interest;
    this.closingBalance = closingBalance;
  }

  /**
   * @return the month
   */
  public int getMonth() {
    return month;
  }

  /**
   * @return the openingBalance
   */
  public Money getOpeningBalance() {
    return openingBalance;
  }

  /**
   * @return the loanRepayment
   */
  public Money getLoanRepayment() {
    return loanRepayment;
  }

  /**
   * @return the interest
   */
  public Money getInterest() {
    return interest;
  }

  /**
   * @return the capitalPayment
   */
  public Money getCapitalPayment() {
    return capitalPayment;
  }

  /**
   * @return the monthlyRepayment
   */
  public Money getMonthlyRepayment() {
    return monthlyRepayment;
  }

  /**
   * @return the closingBalance
   */
  public Money getClosingBalance() {
    return closingBalance;
  }
}