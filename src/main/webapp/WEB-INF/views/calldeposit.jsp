<jsp:include page="header.jsp" flush="true" />

<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="co.za.mpholetsiki.utility.Forecast" %>


<div class="row">
		<div class="col-md-2">
			<form>
			<input type="text" name="principal" placeholder="Capital Amount" >
			<br>
			<br>
			<input type="text" name="interest" placeholder="Interest per annum">
			<br>
			<br>
			<input type="text" name="term" placeholder="Term in months">
			<br>
			<br>
			<input type="submit" name="Forecast" value="forecast" onclick="form.action=''" class="mybutton">
		</form>
		</div>
		
		<div class="col-md-1"> 
			<table style="width: 110%;">
	

			  	<% 
					List<Forecast> forecasts =  (ArrayList<Forecast>)request.getAttribute("forecasts");  
					if(forecasts.size() != 0){ %>
				<tr>
				<th>Month</th>
				<th>Opening Balance</th>
				<th>Interest Earned</th>
				<th>Closing Balance</th>
				</tr> 

				<%
					for (Forecast forecast : forecasts) {
			  	%>
				<tr>
					<td> <%=forecast.getMonth()%> </td>
					<td> <%=forecast.getOpeningBalance()%> </td>
					<td> <%=forecast.getInterest()%> </td>
					<td> <%=forecast.getClosingBalance()%> </td>
				</tr>

				<%
			    }
					}
			  %> 
			</table>
		</div>
	</div>

<jsp:include page="footer.jsp" flush="true" />