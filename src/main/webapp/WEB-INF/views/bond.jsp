<%@include file="/WEB-INF/views/header.jsp" %>
<div>
	<div class="form-section">
		<form class="form">
			<p class="heading">Bond Details</p>

			<hr class="horizontal-rule">
			<div>
				<label id="icon" for="name"><i class="fa fa-money"></i></label>
				<input class="input" type="text" name="capital" id="capital" placeholder=" * Capital Amount" required/>
			</div>
			<div>
				<label id="icon" for="name"><i class="fa fa-line-chart"></i></label>
				<input class="input" type="text" name="rate" id="rate" placeholder=" * Interest (annum)" required/>
			</div>
			<div>
				<label id="icon" for="name"><i class="fa fa-calendar"></i></label>
				<input class="input" type="text" name="period" id="period" placeholder=" * Term (months)" required/>
			</div>
			
			<div class="button-div">
				<input class="submit-button" type="submit" name="forecast" value="forecast" onclick="form.action=''"/>
			</div>
		</form>
		<!-- </hr>
			<p class="section">You can make lump sum payments to your bond during any month of your selected period.</p>
			<p class="section">Add lump sum payments below </p>
			
			</hr>
			<div class="button-div">
				<button onclick="addLumpSumPayment()">+</>
			</div> -->
	</div>
	<div class="table-div">
		<c:if test="${showTable=='true'}">
			<table>
				<tr>
					<th class="table-header">Month</th>
					<th class="table-header">Opening Balance</th>
					<th class="table-header">Loan Repayment</th>
					<th class="table-header">Interest Payment</th>
					<th class="table-header">Capital Payment</th>
					<th class="table-header">Closing Balance</th>
				</tr>
				<c:forEach var="forecast" items="${forecasts}">
					<tr>
		                <td class="table-body">${forecast.month}</td>
		                <td class="table-body">${forecast.openingBalance}</td>
		                <td class="table-body">${forecast.loanRepayment}</td>
		                <td class="table-body">${forecast.interest}</td>
		                <td class="table-body">${forecast.capitalPayment}</td>
		                <td class="table-body">${forecast.closingBalance}</td>
		            </tr>
				</c:forEach>
			</table>
		</c:if>	
	</div>
</div>
<%@include file="/WEB-INF/views/footer.jsp" %>