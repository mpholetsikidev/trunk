
<%@include file="/WEB-INF/views/header.jsp" %>
<div class="main-page-div">
	<div class="section-div">
		<img class="image" src="${pageContext.request.contextPath}/resources/images/calldeposit.png">
		<p class="text">The <b>Call Deposit Calculator</b> featured here is the most convenient way to see how much you can save.</p>
		<!-- <p class="text">There is no simpler or better way to see your money grow.</p> -->
		<p class="text">Simply fill in the required fields and we will do the projections for you</p>
		<a class="link" href="my-servlet/calldeposit/forecast"><i class="fa fa-calculator"></i> Forecast</a>
	</div>
	<div class="section-div">
		<img class="image" src="${pageContext.request.contextPath}/resources/images/bond.png">
		<p class="text">The <b>bond calculator</b> featured here is the most convenient way to see how much you can afford to borrow.</p>
		<!-- <p class="text">There is no simpler or better way to prepare for this important investment.</p> -->
		<p class="text">Simply fill in the required fields and we will do the projections for you</p>
		<a class="link" href="my-servlet/bond/forecast"><i class="fa fa-calculator"></i>  Forecast</a>
	</div>
</div> 
<%@include file="/WEB-INF/views/footer.jsp" %>