<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/fontawesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main.css">
		<title>Finacial Planning</title>
	</head>
		<!-- <body class="main-backgound" background="${pageContext.request.contextPath}/resources/images/background.jpg"> -->
		<div>
			<img class="header-image" src="${pageContext.request.contextPath}/resources/images/header.jpg" alt="Smiley face" height="42" width="42">
		</div>