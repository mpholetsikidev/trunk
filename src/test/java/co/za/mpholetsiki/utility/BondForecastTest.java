/**
 * 
 */
package co.za.mpholetsiki.utility;

import static org.junit.Assert.assertEquals;

import java.util.List;

import co.za.mpholetsiki.service.BondServiceBean;
import org.junit.Before;
import org.junit.Test;

/**
 * @author mpho.letsiki
 *
 */
public class BondForecastTest {

  Money capital;

  Money deposit;

  double rate;

  int period;

  @Before
  public void setCapitalAmount() {
    capital = new Money(1_000_000);
  }

  @Before
  public void setDeposit() {
    deposit = new Money(100_000);
  }

  @Before
  public void setInterestRate() {
    rate = 10;
  }

  @Before
  public void setPreiod() {
    period = 240;
  }

  @Test
  public void shouldCalculateMonthlyInterestBasedOnCapitalRatePeriodThenReturnMoney() {
    BondServiceBean bsb = new BondServiceBean();
    Money monthlyRepayment = bsb.monthlyRepayment(capital, rate, period);
    assertEquals(monthlyRepayment, new Money(9650.22));
  }

  @Test
  public void shouldCalculateMonthlyInterestBasedOnCapitalRatePeriodDepositThenReturnMoney() {
    BondServiceBean bsb = new BondServiceBean();
    Money monthlyRepayment = bsb.monthlyRepayment(capital, rate, period, deposit);
    assertEquals(monthlyRepayment, new Money(8685.19));
  }

  @Test
  public void shouldCalculateBondForecastThenLengthShouldEqualPeriod() {
    BondServiceBean bsb = new BondServiceBean();
    List<Forecast> forecastList = bsb.getForecast(capital, rate, period);
    assertEquals(forecastList.size(), 240);
  }

  @Test
  public void shouldCalculateBondForecastThenFirstOpeningBalanceOfBondSHouldBeEqualToTheCapitalAmount() {
    BondServiceBean bsb = new BondServiceBean();
    List<Forecast> forecastList = bsb.getForecast(capital, rate, period);
    assertEquals(forecastList.get(0).getOpeningBalance(), capital);
  }

  @Test
  public void shouldCalculateBondForecastThenLastClosingBalanceOfBondSHouldBeZero() {
    BondServiceBean bsb = new BondServiceBean();
    List<Forecast> forecastList = bsb.getForecast(capital, rate, period);
    assertEquals(forecastList.get(239).getClosingBalance(), new Money(0));
  }
}