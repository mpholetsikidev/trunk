/**
 * 
 */
package co.za.mpholetsiki.utility;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author mpho.letsiki
 *
 */
public class MoneyTest {

  private Money money1 = new Money(150);

  private Money money2 = new Money(100);

  @Test
  public void shouldAddTwoMoneyObjects() {
    Money m = money1.add(money2);
    assertEquals(m, new Money(250));
  }

  @Test
  public void shouldSubtractAMoneyObjectFromAnotherMoneyObject() {
    Money m = money1.subtract(money2);
    assertEquals(m, new Money(50));
  }

  @Test
  public void shouldDivideMoneyObjectByANumber() {
    Money m = money1.divide(3);
    assertEquals(m, new Money(50));
  }

  @Test
  public void shouldMultipleMoneyObjectByANumber() {
    Money m = money2.multiply(2);
    assertEquals(m, new Money(200));
  }

}